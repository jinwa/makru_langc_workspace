# Workspace for makru_langc
This repository contains tests and other resources helps development of makru_langc.

This repository uses [poetry](https://python-poetry.org) as dependency manager.

You can clone this repository completely by:

````
git clone https://gitlab.com/jinwa/makru_langc_workspace.git
````

## Prepare for working on makru_langc

Software requirements:
- [poetry](https://python-poetry.org)
- [Python](https://python.org) 3.7+
- [Git](https://git-scm.org)

1. Install dependencies:
````
poetry install
````

2. Install git hooks:
````
poetry run pre-commit install
````

3. Clone [makru_langc repository](https://gitlab.com/jinwa/makru_langc), make a link as "makru_langc" in this directory.
````
ln -s ~/Project/makru_langc makru_langc
````

## Testing

We use [pytest](https://pytest.org) as testing framework. It should be installed after `poetry install` run.

## Contribution

While [makru_langc](https://gitlab.com/jinwa/makru_langc) accepting contribution of documents and features, this repository accepts tests and documents which orient to developing.

## License
The MIT License.
