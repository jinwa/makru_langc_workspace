import os
from pathlib import Path
from typing import Dict, Union
from makru.helpers import check_binary

import pytest

from . import run_program_sync

INSTALLED_MAKRU_PLUGINS = {
    "makru_langc": Path(__file__).parent.parent / "makru_langc",
}


@pytest.fixture
def installed_makru_plugins():
    for k in INSTALLED_MAKRU_PLUGINS:
        if not INSTALLED_MAKRU_PLUGINS[k].exists():
            pytest.fail("{} is not a file or directory.".format(str(k)))
    return INSTALLED_MAKRU_PLUGINS


@pytest.fixture
def test_file_name_base(request: pytest.FixtureRequest):
    module_file_name = Path(request.module.__file__)
    name_base: Path
    if module_file_name.is_file():
        name_base = module_file_name.parent
    elif module_file_name.is_dir():
        name_base = module_file_name
    else:
        pytest.fail("could not found base path from {}".format(module_file_name))
    return name_base


@pytest.fixture
def chcwd():
    original_cwd = os.curdir

    def _chcwd_impl(path: Union[str, Path]):
        os.chdir(str(Path(path).absolute()))

    yield _chcwd_impl
    _chcwd_impl(original_cwd)


def apply_folder_template(src: Path, dst: Path):
    for p in src.rglob("*"):
        rel_name = p.relative_to(src).parts
        if rel_name:
            new_name = Path(dst, *rel_name)
            if p.is_dir():
                new_name.mkdir(parents=True)
            else:
                new_name.symlink_to(
                    p
                )  # Rubicon: do not use p.link_to(new_name) -- looks like .symlink_to can handle links between two filesystem, but .link_to can't


def correct_template_name(base: Path, name: str) -> Path:
    return base / name if "/" not in name else Path(name)


@pytest.fixture(scope="function")
def testfolder(
    request: pytest.FixtureRequest,
    tmp_path: Path,
    test_file_name_base: Path,
    chcwd,
    installed_makru_plugins: Dict[str, Path],
):
    folder_template_name_mark = request.node.get_closest_marker("folder_template")
    if folder_template_name_mark is not None:
        folder_template_name = folder_template_name_mark.args[0]
        if folder_template_name is not None:
            folder_template_name = correct_template_name(test_file_name_base, folder_template_name)
            apply_folder_template(folder_template_name, tmp_path)
        if len(folder_template_name_mark.args) == 2:
            mapping = folder_template_name_mark.args[1]
            if not isinstance(mapping, dict):
                pytest.fail("the second argument for folder_template() should be a dict instance.")
            for sub_name in mapping:
                sub_original_name = mapping[sub_name]
                assert isinstance(sub_name, str)
                assert isinstance(sub_original_name, str)
                name_pieces = sub_name.split("/")
                new_name = Path(tmp_path, *name_pieces)
                new_name.mkdir(parents=True)
                apply_folder_template(
                    correct_template_name(test_file_name_base, sub_original_name), new_name
                )

    makru_plugins_folder = tmp_path / "makru" / "plugins"
    for d_name in installed_makru_plugins:
        if not makru_plugins_folder.exists():
            makru_plugins_folder.mkdir(parents=True)
        new_name = makru_plugins_folder / d_name
        original_name = installed_makru_plugins[d_name]
        new_name.symlink_to(original_name, target_is_directory=original_name.is_dir())

    chcwd(str(tmp_path))
    yield tmp_path


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "folder_template(name): use folder 'name' in the same level directory as template to create testing environment.",
    )
    config.addinivalue_line(
        "markers", "force_makru_binary_name(name): use 'name' as binary name of makru."
    )


@pytest.fixture
def makru_binary_name(request: pytest.FixtureRequest):
    forced_name_mark = request.node.get_closest_marker("force_makru_binary_name")
    if forced_name_mark is None:
        return check_binary("makru")
    else:
        forced_name = forced_name_mark.args[0]
        if cp_forced_name := check_binary(forced_name):
            return cp_forced_name
        elif Path(forced_name).exists():
            return forced_name
        else:
            pytest.fail('could not found binary "{}"'.format(forced_name))


@pytest.fixture
def run_makru(testfolder: Path, makru_binary_name: str):
    def _run_makru_impl(args):
        return run_program_sync((makru_binary_name, *args), cwd=str(testfolder))

    return _run_makru_impl
