from pathlib import Path
from py import test
import pytest


class TestTestingEnvironment:
    @pytest.mark.folder_template("folder_template_sample")
    def test_folder_template_can_create_folder_which_have_content(self, testfolder: Path):
        assert (testfolder / "folder1").is_dir()

    @pytest.mark.folder_template("folder_template_sample")
    def test_folder_template_can_create_folder_which_is_empty(self, testfolder: Path):
        assert (testfolder / "empty_folder").is_dir()

    @pytest.mark.folder_template("folder_template_sample")
    def test_folder_template_files_will_be_linked_to_test_folder(self, testfolder: Path):
        assert (testfolder / "folder1" / "file1.txt").is_symlink()

    def test_folder_template_will_not_be_applied_if_no_specified(self, testfolder: Path):
        assert sum(map(lambda x: 1, filter(lambda x: x.name != "makru", testfolder.glob("*")))) == 0

    def test_makru_langc_will_be_installed_when_no_folder_template_applied(self, testfolder: Path):
        assert (testfolder / "makru" / "plugins" / "makru_langc" / "__init__.py").exists()

    @pytest.mark.folder_template("folder_template_sample")
    def test_makru_langc_will_be_installed_when_folder_template_applied(self, testfolder: Path):
        assert (testfolder / "makru" / "plugins" / "makru_langc" / "__init__.py").exists()

    @pytest.mark.folder_template(
        "folder_template_sample",
        {
            "sub_template0": "folder_template_sample",
            "sub_template1": "folder_template_sample",
        },
    )
    def test_sub_templates_can_create_sub_directories(self, testfolder: Path):
        assert (testfolder / "sub_template0").is_dir()
        assert (testfolder / "sub_template1").is_dir()

    @pytest.mark.folder_template(
        None, {"sub_template0": "folder_template_sample", "sub_template1": "folder_template_sample"}
    )
    def test_sub_templates_can_create_sub_directories_when_no_main_template_specified(
        self, testfolder: Path
    ):
        assert (testfolder / "sub_template0").is_dir()
        assert (testfolder / "sub_template1").is_dir()

    def test_testfolder_will_change_current_working_directory_to_temp_directrory(
        self, testfolder: Path
    ):
        assert Path(".").absolute() == testfolder
