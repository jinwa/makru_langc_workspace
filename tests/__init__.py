from io import StringIO
import dataclasses
from dataclasses import dataclass

from makru.helpers import check_binary, shell


@dataclass
class CapturedProcessStatus(object):
    return_code: int = -1
    stdout: StringIO = dataclasses.field(default_factory=lambda: StringIO())


def run_program_sync(args, cwd=None) -> CapturedProcessStatus:
    stat = CapturedProcessStatus()
    stat.return_code = shell(args, cwd=cwd, printf=stat.stdout.write)
    return stat
