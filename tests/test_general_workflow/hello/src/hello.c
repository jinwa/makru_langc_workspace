
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>

char *hello(char *name){
    if (name){
        char *result;
        asprintf(&result, "Hello, %s!", name);
        return result;
    } else {
        return strdup("Hello!");
    }
}
