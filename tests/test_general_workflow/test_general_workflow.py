from tests import CapturedProcessStatus
from typing import Callable, Tuple
import pytest
from pathlib import Path
from io import StringIO


class TestBasicBuilding:
    @pytest.mark.folder_template("asprintf")
    def test_can_build_simple_library(
        self, testfolder: Path, run_makru: Callable[[Tuple[str, ...]], CapturedProcessStatus]
    ):
        stat = run_makru(("-C",))
        print(stat.stdout.getvalue())
        assert stat.return_code == 0
